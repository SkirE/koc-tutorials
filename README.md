# Tutorials Site Generator for KPN Omni Connect

## Introduction

Tutorials and other relevant technical documentation is maintained in this repository and using `mkdocs` in combination
with `mkdocs-kpn` a static website is generated with KPN Style, which is hosted on AWS Amplify.

<!---
## Quickstart

Deploy site directly on AWS Amplify Console

[![amplifybutton](https://oneclick.amplifyapp.com/button.svg)](https://console.aws.amazon.com/amplify/home#/deploy?repo=https://gitlab.com/SkirE/koc-tutorials.git)

- [AWS Amplify Console]

You can use **Password protection** for each branch.

Use the following build specification YML file.

- [amplify.yml]

[AWS Amplify Console]: https://aws.amazon.com/amplify/console/
[amplify.yml]: https://gitlab.com/SkirE/koc-tutorials/-/blob/master/amplify.yml
--->

## Installation on local server

### Clone repository

```bash
git clone https://gitlab.com/SkirE/koc-tutorials.git
cd koc-tutorials
```

### Create virtual environment

```bash
python3 -m venv venv
source ./venv/bin/activate
```

### Install with pip

```bash
pip install -R requirements.txt
```

### Build the site

```bash
python3 -m mkdocs build
```

### Run the site on local server

```bash
python3 -m mkdocs serve
```

### Connect with browser to `http://localhost:8000/`

## What to expect

- Static website in KPN Style containing the tutorials and technical
documentation for KPN Omni Connect
- Responsive design and fluid layout for all kinds of screens and devices,
designed to serve your project documentation in a user-friendly way.
