# Distribution
pip-tools

# Code style
mypy
isort
flake8
black

# PyTest for running the tests.
pytest
pytest-cov
pytest-django
pytest-mock
django-de-testsupport

# Testing tools
mock
django-debug-toolbar
vcrpy
freezegun
