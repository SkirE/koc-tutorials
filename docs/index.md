---
title: Getting Started
summary: A brief introduction to the KPN Omni Connect tutorials and documentation.
created: 2020/12/14 13:20:43
modified: 2020/12/22 14:25:32
author: Erik Schaareman
---
# Getting Started

## Introduction

This website contains tutorials and relevant technical documentation for KPN Omni Connect. The site will be updated frequently and new information will be added to it on a regular base.

## Tutorials

This section contains an overview of all tutorials, which can be published later on a website for customers. The tutorials are grouped per communication channel and it is wise to  Confluence is just being used to maintain the pages itself for now, until we have a better solution to directly publish them online.

1. [SMS Tutorials](./tutorials/sms/1-sms-create-service.md "Create a basic SMS chatbot")
