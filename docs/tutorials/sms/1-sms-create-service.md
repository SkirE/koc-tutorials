---
title: Create Service
summary: Creating a new Service
created: 2020/12/11 14:16:18
modified: 2021/01/11 11:58:28
author: Erik Schaareman
---
# How-to create a new service

## Introduction

With this tutorial we will show you how to create a new service within the KPN Omni Connect platform. A service is a named workspace for managing all necessary configurations and components required to automate a specific customer interaction/customer journey. Every service has a unique service key that is used to authenticate and authorize external triggers including the messaging APIs, custom events, and inbound webhook requests.

Within a service, you can:

* Create and manage flows
* Create and manage rules
* View, copy or regenerate service keys

For each live service, you can see the statistics for:

* Flows Executed
* Messages Sent
* Delivery Rate

The platform provides a default service (first service) named My First Service so that you can get started. You can either start using the default service or create your own services. There is no upper limit on the number of services you can create within an account.

## Audience

This tutorial is useful for developers, consultants or business analysts who are interested in creating communication flows using the visual flow builder on the KPN Omni Connect platform.

## Prerequisites

For this tutorial to work, you need;

* Some basic knowledge about customer journeys and communication processes.
* An account to the KPN Omni Connect platform.

## Video

<!-- markdownlint-disable MD033 -->
<div style="width:100%;height:0px;position:relative;padding-bottom:56.138%;">
  <iframe src="https://www.youtube-nocookie.com/embed/YxgZdGQwMcE" frameborder="0" width="100%" height="100%" allowfullscreen style="width:100%;height:100%;position:absolute;left:0px;top:0px;overflow:hidden;"></iframe>
</div>
<p style="text-align: center; font-style: oblique">
  A video is worth a thousand words — check it out on our <a href="https://www.youtube.com/channel/UCnZ3kMkXSQ0UcVT3McE1-gw">
    YouTube Channel</a><br />
</p>
<!-- markdownlint-enable MD033 -->
<!-- [![Create a service on Omni Connect][video_image]][video] -->

## Creating a service

The platform allows you to build and manage each of your customer journeys in dedicated named workspaces referred to as **'services'**. Using the following steps you are able to create a service.

* You can create a new service by clicking on the **CREATE NEW SERVICE** button on the top right corner of the home page.

    ![Services Home Page][services_home]{: style="width:800px; box-shadow: 5px 5px 3px grey;"}

* Enter a name for the service that represents a specific interaction or customer journey. For example; *Appointment Reminder*

    ![Create New Service][services_new]{: style="width:300px; box-shadow: 5px 5px 3px grey;"}

* After you have created the new service, the *Service Dashboard* of the newly created service is opened and you have successfully created a new service

    ![Service Dashboard][services_dashboard]{: style="width:800px; box-shadow: 5px 5px 3px grey;"}

## Service Dashboard

Once you create a new service or click the name of any of the existing services on the home page, you reach the service-specific dashboard. The service dashboard provides an overview of traffic stats for each of the channels within use in that service along with the flow execution counts and messaging API stats for the last 30 days. The service key with which you need to authenticate your messaging APIs, custom events, and inbound webhook requests is also available on the service dashboard. In addition to the service key, the platform supports JWT Authentication for using Messaging APIs. The JWT authentication tokens for using Messaging API are also available within the service under **Settings** section.

Further, you can create flows, rules, buy numbers, and configure apps from the service dashboard.

For detailed information about the performance of the service, access various reports.

* **API - JTW Credentials** \- This section of the dashboard contains the credentials to be used when calling the Messaging and Event APIs that are part of this service

* **Flows (top)** \- This section contains the flows you build, using templates or a complete new flow. It gives also access to the option to buy numbers for SMS and voice interactions, configure app assets or view the documentation. These options are also available from the side menu.

* **Traffic** \- This section contains an overview of outbound messages and the delivery rates of the last 30 days per communication channels used within the different flows of this service.

* **Flows (right)** \- This section contains an overview of the number of flow executions and messages sent within the last 30 days across all flows within the service.

* **Messaging API** \- This section contains an overview of the number of flow API requests and messages sent within the last 30 days across all APIs within the service.

[services_home]: img/1/142021705.png "Service Home"
[services_new]: img/1/142021716.png "Create Service"
[services_dashboard]: img/1/142021852.png "Service Dashboard"
<!-- [video_image]: https://img.youtube.com/vi/YxgZdGQwMcE/0.jpg "Create a new service" -->
<!-- [video]: https://www.youtube-nocookie.com/embed/YxgZdGQwMcE "Tutorial Video: How to create a new service" -->
