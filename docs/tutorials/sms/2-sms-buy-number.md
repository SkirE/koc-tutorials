---
title: Buy Number
summary: Creating a new Service
created: 2020/11/16 12:00:00
modified: 2020/12/22 15:59:39
author: Erik Schaareman
---

# 2. How-to buy a number

## Introduction

With this tutorial we will show you how to buy a new phone number within the KPN Omni Connect platform to make and/or receive calls, send and/or receive SMSes, or to be able to use certain apps available within the platform, you must associate the services with the right type of phone number. Phone numbers can be either purchased or rented for a monthly fee.

Phone numbers are of four types:

* **Multi-Function (Voice, SMS)** \- have both SMS and voice capabilities
* **Landline (Voice)** \- have only voice capability
* **Mobile (SMS)** \- have only SMS capability
* **Tollfree (Voice and SMS)** \- have SMS and/or voice capabilities.

!!! note "Number Availability"
    The type of phone numbers available in different countries may vary depending on the local government regulations and restrictions. If you cannot find a phone number for a certain country, reach out to the us.

## Audience

This tutorial is useful for developers, consultants or business analysts who are interested in creating communication flows using the visual flow builder on the KPN Omni Connect platform.

## Prerequisites

For this tutorial to work, you need;

* Some basic knowledge about customer journeys and communication processes.
* An account to the KPN Omni Connect platform.

## Video (in Dutch)

<!-- markdownlint-disable MD033 -->
<div style="width:100%;height:0px;position:relative;padding-bottom:56.138%;">
  <iframe src="https://www.youtube-nocookie.com/embed/l-P6SbUbjA8" frameborder="0" width="100%" height="100%" allowfullscreen style="width:100%;height:100%;position:absolute;left:0px;top:0px;overflow:hidden;"></iframe>
</div>
<p style="text-align: center; font-style: oblique">
  A video is worth a thousand words — check it out on our <a href="https://www.youtube.com/channel/UCnZ3kMkXSQ0UcVT3McE1-gw">
    YouTube Channel</a><br />
</p>
<!-- markdownlint-enable MD033 -->

## Buying a phone number

To be able to send/receive SMS messages and/or handle incoming/outgoing calls, you will need to have a phone number. Within the platform you can purchase phone numbers for 84 countries. For this tutorial we will focus on buying a Dutch phone number for SMS.

* There are 2 ways you can use to buy a phone number;

    1. In the side menu select **Assets > Numbers**
    ![Navigate to Numbers][navigate]{: style="width:150px; box-shadow: 5px 5px 3px grey; display:block;"}

    2. Or select one of the services and in the displayed **Service Dashboard** select the option **Buy Numbers** (bottom of the Flows section)
    ![Buy Numbers][numbers_buy]{: style="width:800px; box-shadow: 5px 5px 3px grey;"}

* Choose the **Phone Number** option from the **Get Numbers** drop-down list box.  

![Get Numbers][numbers_get]{: style="width:800px; box-shadow: 5px 5px 3px grey;"}

* Select the **Country** (e.g. Netherlands), **Number Type** (e.g. Multi-Function, and click **Find Numbers**.  

![Find Numbers][numbers_find]{: style="width:800px; box-shadow: 5px 5px 3px grey;"}

!!! note "**Unavailability**"
    If the country you are looking for is not available in the list, reach out to the us. Also note that due to restrictions and regulations not all type of phone numbers are available in each country.

* Select the desired phone number (e.g. 3197010240073) and click on the **Buy** button.  

* An confirmation message is shown with the details of the phone number and the rates. Click on **Buy Number** to confirm to purchase.  

![Buy Number][buy]{: style="width:400px; box-shadow: 5px 5px 3px grey;"}

* In the overview with phone numbers, the new purchased phone number is now visible. When building any flow, you are able to select the purchased flow numbers as the originating number. This will be explained in detail in tutorials about creating communication flows.  

![Number Overview][overview]{: style="width:800px; box-shadow: 5px 5px 3px grey;"}

[navigate]: img/2/142024768.png "Navigate to Numbers"
[numbers_buy]: img/2/142024784.png "Buy Numbers"
[numbers_get]: img/2/142024837.png "Get Numbers"
[numbers_find]: img/2/142024843.png "Find Numbers"
[buy]: img/2/142024855.png "Buy Number"
[overview]: img/2/142024862.png "Overview of Numbers"
